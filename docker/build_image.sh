#!/usr/bin/env sh

set -e

IMAGE_BASE=${CI_REGISTRY_IMAGE:-fava}
IMAGE_NAME="fava"
COMMIT_REF=${CI_COMMIT_REF_NAME:-unstable}
IMAGE_TAG=$(cat "docker/VERSION")

if [ "$COMMIT_REF" = "leipert-main" ]; then
    DOCKER_IMAGE="$IMAGE_BASE/$IMAGE_NAME:$IMAGE_TAG"
else
    IMAGE_POSTFIX=${CI_COMMIT_SHORT_SHA:-unstable}
    DOCKER_IMAGE="$IMAGE_BASE/unstable/$IMAGE_NAME:$IMAGE_TAG-$IMAGE_POSTFIX"
fi

if [ "$CI" = "true" ]; then
    echo "Running on CI, so we check if the image already exists"
    if docker manifest inspect "$DOCKER_IMAGE" >/dev/null; then
        echo "Image $DOCKER_IMAGE already exists"
        exit 0
    fi
fi

echo "Building image $DOCKER_IMAGE"

docker build --tag "$DOCKER_IMAGE" --file docker/Dockerfile .

if [ "$CI" = "true" ]; then
    echo "Running on CI, so we push the built image"
    docker push "$DOCKER_IMAGE"
    if [ "$COMMIT_REF" = "leipert-main" ]; then
        docker tag "$IMAGE_BASE/$IMAGE_NAME:$IMAGE_TAG" "$IMAGE_BASE/$IMAGE_NAME:latest"
        docker push "$IMAGE_BASE/$IMAGE_NAME:latest"
    fi
fi
